Функционал чекбоксов страницы "Прайс лист" 

* Тестовые данные:
1. Тестовый сервер - https://test2.stroyrem-nn.ru/
2. Продовый сервер -https://stroyrem-nn.ru/

* Предусловия:
1. Нажать кнопку "Прайс" (header (desktop )) / бургер-меню (тач)

* Шаги:
1. У каждого раздела кликнуть чекбокс.

* Ожидаемый результат:
В каждом чекбоксе появилась "галочка".

2. У каждого раздела кликнуть чекбокс.

* Ожидаемый результат:
В каждом чекбоксе скрылась "галочка".

3. Отметить чекбокс любого раздела.
4. В хлебных крошках нажимаем на "Главная".
5. Нажать кнопку "назад" браузера.

* Ожидаемый результат:
Отмеченный чекбокс сохранился.

Автор: Н.Краснобородько


Тестовый сервер

| Дата | Время | Версия браузера Десктоп | Результат/Баг в Трелло Десктоп | Версия браузера и ОС Тач | Результат/Баг в Трелло Тач | Дата релиза | QA  |
| --- | --- | --- | --- | --- | --- | --- | --- |
| 2023-10-09 | 16:20 | Chrome 117.0.5938.150 Yandex 23.9.0.2272 | PASS |Samsung Galaxy A50/Chrome 117.0.5938.153  | FAIL https://trello.com/c/rVQW8cxt/539 | 08.10.23 | Наталья К. |

Продовый сервер

| Дата | Время | Версия браузера Десктоп | Результат/Баг в Трелло Десктоп | Версия браузера и ОС Тач | Результат/Баг в Трелло Тач | Дата релиза | QA  |
| --- | --- | --- | --- | --- | --- | --- | --- |
| 2023-10-09 | 16:30 | Chrome 117.0.5938.150 Yandex 23.9.0.2272 | PASS |Samsung Galaxy A50/Chrome 117.0.5938.153  | FAIL https://trello.com/c/rVQW8cxt/539 | 08.10.23 | Наталья К. |