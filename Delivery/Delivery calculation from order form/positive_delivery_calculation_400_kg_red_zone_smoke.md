#Доставка
# Доставка товара весом до 400 кг. в красной зоне
#Smoke

### Доставка товара весом до 400 кг. в красной зоне

- Тестовые данные:
- Предусловие:
1. Рассчитать расстояние от склада Стройрема до точки в красной зоне - Нижегородская область, Дзержинск, улица Гагарина, 23 (40 км)
2. Открыть главную страницу сайта: тест сервер - https://test2.stroyrem-nn.ru/ , прод сервер - https://stroyrem-nn.ru/
3. В строке поиска ввести "Штукатурка гипсовая AKSOLIT М50 30 кг машинного нанесения"
4. В открывшемся окне выбрать Штукатурка гипсовая AKSOLIT М50 30 кг машинного нанесения
5. Добавить 13 шт. в корзину
6. Нажать на кнопку "Корзина"
7. В открывшемся окне нажать "Перейти к оформлению"
8. В поле "ФИО" написать Тест
9. В поле "Телефон" написать +7(999)999-99-99
10. В поле "Email" написать example@gmail.com
11. Нажать кнопку "Продолжить"
- Шаги:
1. В поле город ввести:  “Держинск”
2. В поле адрес ввести: “улица Гагарина 23”
- Ожидаемый результат: в поле вариант доставки отображаются следующая цена: 600+60*40 = 3000

Автор: Екатерина

* Тестовый сервер

 
|  №  | Дата       | Время |           Версия браузера/Десктоп          |        Результат/Баг в Трелло Десктоп    |             Версия браузера и ОС Тач      |           Результат/Баг в Трелло Тач          |  Дата Релиза  |  Имя   |
| --- | ---------- | ----- |-------------------------------------| ---------------------------------- | ---------------------------------- | ---------------------------------- | ------| ------  |
| 1   | 2023-08-04 | 16:14 |Chrome 116.0.5845.97 Yandex 23.7.2.765| PASS | Xiaomi Mi 9 Lite MIUI 12.5.2       | PASS  | 04.07 | Екатерина  |



* Продовый сервер


|  №  | Дата       | Время |           Версия браузера/Десктоп          |        Результат/Баг в Трелло Десктоп    |             Версия браузера и ОС Тач      |           Результат/Баг в Трелло Тач          |  Дата Релиза  |  Имя   |
| --- | ---------- | ----- |-------------------------------------| ---------------------------------- | ---------------------------------- | ---------------------------------- | ------| ------  |
| 1   | 2023-08-04 | 16:20 |Chrome 116.0.5845.97 Yandex 23.7.2.765| PASS | Xiaomi Mi 9 Lite MIUI 12.5.2       | PASS  | 04.07 | Екатерина  |
| 2   | 2023-08-27 | 11:20 |Chrome 116.0.5845.97 Yandex 23.7.3.823| PASS | Xiaomi Mi 9 Lite MIUI 12.5.2       | PASS  | 27.08 | Сабина  |
| 3   | 2023-09-17 | 14.24 |Chrome 116.0.5845.97 | FAILED [https://trello.com/c/0sq41GFH] | Iphone, Chrome  | FAILED [https://trello.com/c/0sq41GFH] | 2023-09-17 | Виктор |
