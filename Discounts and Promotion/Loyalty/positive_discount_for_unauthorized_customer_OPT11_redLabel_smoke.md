Не авторизован, ОПТ11, скидка на товар с красной ценой.

- Тестовые данные: 
  
  Клубная карта: карта категории ОПТ11

- Предусловие:

1. Пользователь не авторизован
2. Есть клубная карта категории ОПТ11
3. Открыта страница с товарами красной цены:

тест сервер - https://test2.stroyrem-nn.ru/catalog/action

прод сервер - https://stroyrem-nn.ru/catalog/action

- Шаги:
  
  1.Выбрать товар с минимальным ценником, запомнить цену. 
  
  2.Нажать на этом товаре кнопку "В корзину".
  
  3.Перейти в Корзину.
  
  4.В поле ввода клубной карты ввести ее номер и нажать кнопку: "Получить скидку"
  
  5.Проверить сумму за товар и скидку.

- Ожидаемый результат:
  
  1. Сумма за товар в корзине и в блоке отображаются со скидкой 2,25%.
  
Автор: Валерий

- Тестовый сервер

| Дата | Время | Версия браузера Десктоп | Результат/Баг в Трелло Десктоп|  Версия браузера и ОС Тач |Результат/Баг в Трелло Тач| Дата релиза| QA  |
| --- | --- | --- | --- |  --- | --- | --- | --- |   
|2023-08-09 | 18:11 | Chrome 115.0.5790.171 | FAILED https://trello.com/c/5BEWhx8o ||||  |
|||||||||
|2023-09-28 | 11:59 | Chrome 117.0.5938.89 | FAIL https://trello.com/c/5BEWhx8o | Safari 16.6.1 | FAIL https://trello.com/c/5BEWhx8o | --- | Мария |

- Продовый сервер

| Дата | Время | Версия браузера Десктоп | Результат/Баг в Трелло Десктоп|  Версия браузера и ОС Тач |Результат/Баг в Трелло Тач| Дата релиза| QA  |
| --- | --- | --- | --- |  --- | --- | --- | --- |   
|||||||||
