* Тестовые данные: Позитивная проверка. Карточка выполненного заказа - перейти в “Заказ” через номер заказа (touch).

	тестовый сервер - https://test2.stroyrem-nn.ru/   продовый сервер - https://stroyrem-nn.ru/

* Предусловие: пользователь авторизован, открыт «Личный кабинет» / "Профиль", у пользователя есть выполненный заказ

* Шаги:
1.	В боковом меню нажать на "Историю заказов"
2.	В разделе "Выполненные" в карточке заказа нажать на номер заказа "№ХХХХХХХХ >" 

* Ожидаемый результат: переходим в "Заказ"

* Постусловие: удалить тестовые данные

Автор: Надежда
 	
* Отчет о тестировании
  
Тестовый сервер
| Дата | Время | Версия браузера Десктоп | Результат/Баг в Трелло Десктоп|  Версия браузера и ОС Тач |Результат/Баг в Трелло Тач| Дата релиза| QA  |
| --- | --- | --- | --- |  --- | --- | --- | --- |   
| 11.10.23 | 08:00 | --- | --- | Chrome версия 117.0.5938.60 MIUI 12.5.13 | PASS | 01.10.23 | Надежда |

Продовый сервер
| Дата | Время | Версия браузера Десктоп | Результат/Баг в Трелло Десктоп|  Версия браузера и ОС Тач |Результат/Баг в Трелло Тач| Дата релиза| QA |
| --- | --- | --- | --- |  --- | --- | --- | --- |   
| 11.10.23 | 08:00 | --- | --- | Chrome версия 117.0.5938.60 MIUI 12.5.13 | PASS | 01.10.23 | Надежда |