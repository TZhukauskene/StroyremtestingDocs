* Тестовые данные: Позитивная проверка. Карточка текущего заказа - “Заказ” -  “Доставка”-  совпадение с макетом (touch).

	тестовый сервер - https://test2.stroyrem-nn.ru/   продовый сервер - https://stroyrem-nn.ru/

* Предусловие: пользователь авторизован, открыт «Личный кабинет» / "Профиль", у пользователя есть текущий заказ со способом доставки "Доставка"

* Шаги:
1.	В боковом меню нажать на "Историю заказов"
2.	В разделе "Текущие" в карточке заказа со способом доставки "Доставка" нажать на "Перейти к заказу"

* Ожидаемый результат: переходим в "Заказ", где есть:
	- таб со статусом заказа (один из): "Заказ оформлен", "Принят в работу", "Весь товар на складе", "Заказ готов к выдаче", "Выполнен"
	- блок "Доставка", где есть: 
		- дата и временной интервал доставки
		- адрес доставки
		- вид доставки (один из): "Разгружу сам" / "Выгрузить из машины" / "Подъём в квартиру"
	- блок с информацией о заказе, где есть:
		- информация о товаре: "Вес", "Товары на сумму", "Доставка", "Сумма заказа"
		- информация об оплате: "Оплачено" / "Не оплачено" и способ оплаты (один из): "При получении наличными", "Картой на сайте", "Предоплата в офисе"
		- кнопка "Оплатить заказ сейчас"
	- блок "Список покупок" с карточками купленных товаров, где есть:
		- фото товара
		- наименование товара
		- итоговая цена (например, 2300 ₽)		
		- расчёт: цена за единицу х 1 ед. (например, 23 ₽ х 100 шт.)	
		- стрелка >
	- блок "Получатель", где есть:
		- аватар с первой буквой имени плательщика
		- имя плательщика
		- номер телефона плательщика
		- e-mail плательщика
	- блок "Остались вопросы по заказу?" с кнопкой "Заказать обратный звонок"
	
* Постусловие: удалить тестовые данные

Автор: Надежда
 	
* Отчет о тестировании
  
Тестовый сервер
| Дата | Время | Версия браузера Десктоп | Результат/Баг в Трелло Десктоп|  Версия браузера и ОС Тач |Результат/Баг в Трелло Тач| Дата релиза| QA  |
| --- | --- | --- | --- |  --- | --- | --- | --- |   
| 29.09.23 | 01:10 | --- | --- | Chrome версия 117.0.5938.60 MIUI 12.5.13 | FAIL https://trello.com/c/ZlKBN7Oq/587   https://trello.com/c/MMiz6K5O/588 | 17.09.23 | Надежда |
| 05.10.23 | 08:00 | --- | --- | Chrome версия 117.0.5938.60 MIUI 12.5.13 | FAIL https://trello.com/c/ZlKBN7Oq/587   https://trello.com/c/MMiz6K5O/588 | 01.10.23 | Надежда |
| 11.10.23 | 08:00 | --- | --- | Chrome версия 117.0.5938.60 MIUI 12.5.13 | FAIL https://trello.com/c/MMiz6K5O/588 | 01.10.23 | Надежда |

Продовый сервер
| Дата | Время | Версия браузера Десктоп | Результат/Баг в Трелло Десктоп|  Версия браузера и ОС Тач |Результат/Баг в Трелло Тач| Дата релиза| QA |
| --- | --- | --- | --- |  --- | --- | --- | --- |   
| 29.09.23 | 01:15 | --- | --- | Chrome версия 117.0.5938.60 MIUI 12.5.13 | FAIL https://trello.com/c/MMiz6K5O/588 | 17.09.23 | Надежда |
| 01.10.23 | 09:08 | --- | --- | Chrome версия 117.0.5938.60 MIUI 12.5.13 | FAIL https://trello.com/c/MMiz6K5O/588 | 01.10.23 | Надежда |
| 08.10.23 | 08:00 | --- | --- | Chrome версия 117.0.5938.60 MIUI 12.5.13 | FAIL https://trello.com/c/UgS8Uw1W/622 | 01.10.23 | Надежда |