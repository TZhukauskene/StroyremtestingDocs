* Тестовые данные: Позитивная проверка. Информационное модальное окно “i” - совпадение с макетом (touch).

	тестовый сервер - https://test2.stroyrem-nn.ru/   продовый сервер - https://stroyrem-nn.ru/

* Предусловие: пользователь авторизован, открыт «Личный кабинет» / "Профиль", у пользователя есть заказы

* Шаги:
1.	В боковом меню нажать на "Историю заказов"
2.	Нажать на иконку информации ("i")
	* Ожидаемый результат: открывается модальное окно с сообщением "Нажмите на заказ для получения более подробной информации" и кнопкой "Понятно"
	
3.	Нажать на кнопку "Понятно"
	* Ожидаемый результат: модальное окно закрывается
	
* Постусловие: удалить тестовые данные

Автор: Надежда
 	
* Отчет о тестировании
  
Тестовый сервер
| Дата | Время | Версия браузера Десктоп | Результат/Баг в Трелло Десктоп|  Версия браузера и ОС Тач |Результат/Баг в Трелло Тач| Дата релиза| QA  |
| --- | --- | --- | --- |  --- | --- | --- | --- |   
| 28.09.23 | 17:30 | --- | --- | Chrome версия 117.0.5938.60 MIUI 12.5.13 | PASS | 17.09.23 | Надежда |
| 05.10.23 | 08:00 | --- | --- | Chrome версия 117.0.5938.60 MIUI 12.5.13 | PASS | 01.10.23 | Надежда |

Продовый сервер
| Дата | Время | Версия браузера Десктоп | Результат/Баг в Трелло Десктоп|  Версия браузера и ОС Тач |Результат/Баг в Трелло Тач| Дата релиза| QA |
| --- | --- | --- | --- |  --- | --- | --- | --- |   
| 28.09.23 | 17:32 | --- | --- | Chrome версия 117.0.5938.60 MIUI 12.5.13 | PASS | 17.09.23 | Надежда | 
| 01.10.23 | 08:00 | --- | --- | Chrome версия 117.0.5938.60 MIUI 12.5.13 | PASS | 01.10.23 | Надежда | 
| 08.10.23 | 08:00 | --- | --- | Chrome версия 117.0.5938.60 MIUI 12.5.13 | PASS | 01.10.23 | Надежда | 