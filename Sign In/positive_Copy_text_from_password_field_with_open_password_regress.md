Копирование текста из открытого поля пароль.

* Тестовые данные:текст(Test100723!)

* Предварительные шаги:
1. Открыть форму авторизации на сайте Стройрем

* Шаги:
1. Ввести текст в поле пароль
2. Нажать кнопку "Показать пароль"
3. Скопировать текст из поля пароль

* Ожидаемый результат:
1. Скопированные данные находятся в буфере обмена

* Постусловие:Удалить тестовые данные

Автор: Александр Воронин

* Тестовый сервер

 
|  №  | Дата       | Время |           Версия браузера/Десктоп          |        Результат/Баг в Трелло Десктоп    |             Версия браузера и ОС Тач      |           Результат/Баг в Трелло Тач          |  Дата Релиза  |  Имя   |
| --- | ---------- | ----- |-------------------------------------| ---------------------------------- | ---------------------------------- | ---------------------------------- | ------| ------  |
| 1   | 2023-07-18 | 22:02 |Chrome 116.0.5845.97 Mazila 116.0.2  | PASS                               | Chrome 116.0.5845.97               | PASS                               | 04.07 | Александр Воронин  |
| 2   | 2023-08-13 | 21:53 |Chrome 116.0.5845.97 Yandex 23.7.2.765| PASS                              | Chrome 116.0.5845.97               | PASS                               | 13.08 | Сабина  |
|?|2023-09-28 | 13:54 | Chrome 115.0.5790.171 Firefox 115.0.3 | PASSED  | Chrome 115.0.5790.166, Android 13 | PASSED  |28.09.23 | Валерий|
| 4 |2023-10-05 | 19:04 | Chrome 117.0.5938.89 | PASSED | Safari 16.6.1 | PASSED | --- | Мария | 
* Продовый сервер


|  №  | Дата       | Время |           Версия браузера/Десктоп          |        Результат/Баг в Трелло Десктоп    |             Версия браузера и ОС Тач      |           Результат/Баг в Трелло Тач          |  Дата Релиза  |  Имя   |
| --- | ---------- | ----- |-------------------------------------| ---------------------------------- | ---------------------------------- | ---------------------------------- | ------| ------  |
| 1   | 2023-07-18 | 16:41 |Chrome 116.0.5845.97 Mazila 116.0.2  | PASS                               | Chrome 116.0.5845.97               | PASS                               | 04.07 | Александр Воронин  |
| 2   | 2023-08-13 | 21:54 |Chrome 116.0.5845.97 Yandex 23.7.2.765| PASS                              | Chrome 116.0.5845.97               | PASS                               | 13.08 | Сабина  |
|01.10.23|01.10.23 | 13:54 | Opera One(версия: 102.0.4880.70) | PASSED  | OperaMini 73.0.2254.68181, Android 13 | PASSED  |01.10.23 | Валерий|

