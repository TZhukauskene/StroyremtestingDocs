Выбор товара в разделе ВИД на странице "Штукатурные смеси".

* Тестовые данные: 
1. Тестовый сервер - https://test2.stroyrem-nn.ru/
2. Продовый сервер -https://stroyrem-nn.ru/

* Предусловия:
1. Открыта страница "Штукатурные смеси" https://test2.stroyrem-nn.ru/catalog/shtukaturnye-smesi (или https://stroyrem-nn.ru/catalog/shtukaturnye-smesi)


Шаги:
1. Нажать кнопку "Фильтр"(тач)
2. В списке ВИД нажать первую кнопку с названием.
3. Нажать кнопку "Показать  ... товаров" (тач)

* Ожидаемый результат:
Отображен товар (штукатурные смеси) выбранного вида.

1. Нажать кнопку "Фильтр"(тач)
2. В списке ВИД нажать последнюю кнопку с названием.
3. Нажать кнопку "Показать  ... товаров" (тач)

* Ожидаемый результат:
Отображен товар (штукатурные смеси) выбранных видов.

1. Нажать кнопку "Фильтр"(тач)
2. В списке ВИД нажать первую кнопку с названием.
3. Нажать кнопку "Показать  ... товаров" (тач)

* Ожидаемый результат:
Отображен товар (штукатурные смеси) выбранного вида.

1. Нажать кнопку "Фильтр"(тач)
2. В списке ВИД нажать последнюю кнопку с названием.
3. Нажать кнопку "Показать  ... товаров" (тач)

* Ожидаемый результат:
Отображен весь товар (штукатурные смеси).

Автор: В.Савин


* Тестовый сервер 

| Дата | Время | Браузер Desktop| Результат/Баг № Trello| Браузер тач| Результат/Баг № Trello| Дата релиза |Имя |
| --- | --- | --- | --- | --- | --- | --- | --- | 
|2023-07-27 | 12:07 | Chrome 114.0.5735.248 | PASS | Samsung Galaxy A50/Chrome 114.0.5735.196 | PASS | 04.07.23 | Наталья К. | 
|2023-07-27 | 12:10 | Yandex 23.7.0.2534 | PASS |  |  | 04.07.23 | Наталья К. |
| 13.08.23 | 01:23 | Chrome версия 114.0.5735.199 Firefox версия 115.0.2 | PASS | Chrome версия 114.0.5735.196 MIUI 12.5.13 | PASS | 13.08.23 | Надежда | 
| 2023-10-01 | 12:10 | Chrome 117.0.5938.132 Yandex 23.9.0.2272 | PASS |Samsung Galaxy A50/Chrome 117.0.5938.60  | PASS | 01.10.23 | Наталья К. | 


* Продовый сервер

| Дата | Время | Браузер Desktop| Результат/Баг № Trello| Браузер тач| Результат/Баг № Trello| Дата релиза |Имя |
| --- | --- | --- | --- | --- | --- | --- | --- | 
| 2023-07-27 | 12:08 | Chrome 114.0.5735.248 | PASS | Samsung Galaxy A50/Chrome 114.0.5735.196 | PASS | 04.07.23 | Наталья К. | 
| 2023-07-27 | 12:11 | Yandex 23.7.0.2534 | PASS |  |  | 04.07.23 | Наталья К. |
| 13.08.23 | 23:15 | Chrome версия 114.0.5735.199 Firefox версия 115.0.2 | PASS | Chrome версия 114.0.5735.196 MIUI 12.5.13 | PASS | 13.08.23 | Надежда |  
|2023-09-17 | 10.59 | Chrome | PASSED | Chrome, iphone | PASSED | 2023-09-17 | Виктор
| 2023-10-01 | 12:10 | Chrome 117.0.5938.132 Yandex 23.9.0.2272 | PASS |Samsung Galaxy A50/Chrome 117.0.5938.60  | PASS | 01.10.23 | Наталья К. |
| 2023-10-08 | 07:00 | Chrome 117.0.5938.150 Yandex 23.9.0.2272 | PASS |Samsung Galaxy A50/Chrome 117.0.5938.153  | PASS | 08.10.23 | Наталья К. |

