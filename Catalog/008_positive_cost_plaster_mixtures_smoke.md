Фильтрация по цене страницы "Штукатурные смеси".

* Тестовые данные: 
1. Тестовый сервер - https://test2.stroyrem-nn.ru/
2. Продовый сервер -https://stroyrem-nn.ru/

* Предусловия:
Открыта страница "Штукатурные смеси" https://test2.stroyrem-nn.ru/catalog/shtukaturnye-smesi (или https://stroyrem-nn.ru/catalog/shtukaturnye-smesi)

* Шаги:
Нажать ссылку "По цене по убыванию"

*Ожидаемый результат:
Список материалов упорядочился по убыванию.

* Шаги:
Нажать ссылку "По цене по возрастанию"

*Ожидаемый результат:
Список материалов упорядочился по возрастанию.

* Постусловие: удалить тестовые данные

Автор: В.Савин


* Тестовый сервер 

| Дата | Время | Браузер Desktop| Результат/Баг № Trello| Браузер тач| Результат/Баг № Trello| Дата релиза |Имя |
| --- | --- | --- | --- | --- | --- | --- | --- | 
|2023-07-21 | 18:27 | Chrome 114.0.5735.199 | FAIL https://trello.com/c/SJDsOxck/45 | Samsung Galaxy A50/Chrome 114.0.5735.196 | FAIL https://trello.com/c/SJDsOxck/45 | 04.07.23 | Наталья К. | 
|2023-07-21 | 18:30 | Yandex 23.5.4.674 | FAIL https://trello.com/c/SJDsOxck/45 |  |  | 04.07.23 | Наталья К. |
 12.08.23 | 11:20 | Chrome версия 114.0.5735.199 Firefox версия 115.0.2 | FAIL https://trello.com/c/QCTu8oOS/391 | Chrome версия 114.0.5735.196 MIUI 12.5.13 | FAIL https://trello.com/c/QCTu8oOS/391 | 16.06.23 | Надежда |
| 2023-09-28 | 18:40 | Chrome 117.0.5938.92 Yandex 23.7.5.739 | FAIL https://trello.com/c/QCTu8oOS/391 |Samsung Galaxy A50/Chrome 117.0.5938.60  | FAIL https://trello.com/c/QCTu8oOS/391 | 17.09.23 | Наталья К. | 
| 2023-10-06 | 14:33 | Chrome 117.0.5938.150 Firefox 118.0.1 | PASS| Chrome 117.0.5938.60, Android 10 |PASS| 01.10.2023  | Татьяна |


Продовый сервер

| Дата | Время | Браузер Desktop| Результат/Баг № Trello| Браузер тач| Результат/Баг № Trello| Дата релиза |Имя |
| --- | --- | --- | --- | --- | --- | --- | --- | 
| 2023-07-21 | 18:28 | Chrome 114.0.5735.199 | FAIL https://trello.com/c/SJDsOxck/45 | Samsung Galaxy A50/Chrome 114.0.5735.196 | FAIL https://trello.com/c/SJDsOxck/45 | 04.07.23 | Наталья К. | 
| 2023-07-21 | 18:31 | Yandex 23.5.4.674 | FAIL https://trello.com/c/SJDsOxck/45 |  |  | 04.07.23 | Наталья К. |
 13.08.23 | 20:10 | Chrome версия 114.0.5735.199 Firefox версия 115.0.2 | FAIL https://trello.com/c/QCTu8oOS/391 | Chrome версия 114.0.5735.196 MIUI 12.5.13 | FAIL https://trello.com/c/QCTu8oOS/391 | 13.08.23 | Надежда |  
|2023-09-17 | 08:23 | Chrome | PASS | Chrome, iphone | PASS | 2023-09-17 | Виктор
| 2023-10-01 | 9:35 | Chrome 117.0.5938.132 Yandex 23.9.0.2272 | FAIL https://trello.com/c/QCTu8oOS/391 |Samsung Galaxy A50/Chrome 117.0.5938.60  | FAIL https://trello.com/c/QCTu8oOS/391 | 01.10.23 | Наталья К. |
| 2023-10-08 | 07:00 | Chrome 117.0.5938.150 Yandex 23.9.0.2272 | FAIL https://trello.com/c/QCTu8oOS/391 |Samsung Galaxy A50/Chrome 117.0.5938.153  | FAIL https://trello.com/c/QCTu8oOS/391 | 08.10.23 | Наталья К. |
