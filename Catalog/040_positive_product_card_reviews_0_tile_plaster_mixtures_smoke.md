Переход к отзывам о товаре на странице "Штукатурные смеси".

* Тестовые данные: 

Priority = middle

1. Тестовый сервер - https://test2.stroyrem-nn.ru/
2. Продовый сервер -https://stroyrem-nn.ru/

* Предусловия:
1. Открыта страница "Штукатурные смеси" https://test2.stroyrem-nn.ru/catalog/shtukaturnye-smesi (или https://stroyrem-nn.ru/catalog/shtukaturnye-smesi)
2. Кнопка отображения "плитка" активирована.

Шаги:
1. Нажать на отзывы (*****), где их не оставляли.

* Ожидаемый результат:
Произошел переход к отзывам выбранного товара.

2. Нажать кнопку "назад" браузера.

* Ожидаемый результат:
Произошел возврат на предыдущую страницу.

Автор: В.Савин


* Тестовый сервер 

| Дата | Время | Браузер Desktop| Результат/Баг № Trello| Браузер тач| Результат/Баг № Trello| Дата релиза |Имя |
| --- | --- | --- | --- | --- | --- | --- | --- | 
|2023-07-29 | 20:27 | Chrome 114.0.5735.248 | FAIL https://trello.com/c/H9BpaAV0/300 | Samsung Galaxy A50/Chrome 114.0.5735.196  | FAIL https://trello.com/c/H9BpaAV0/300 | 04.07.23 | Наталья К. | 
|2023-07-29 | 20:30 | Yandex 23.7.0.2534 | FAIL https://trello.com/c/H9BpaAV0/300 |  |  | 04.07.23 | Наталья К. |
| 13.08.23 | 01:56 | Chrome версия 114.0.5735.199 Firefox версия 115.0.2 | FAIL https://trello.com/c/H9BpaAV0/300 | Chrome версия 114.0.5735.196 MIUI 12.5.13 | FAIL https://trello.com/c/H9BpaAV0/300 | 13.08.23 | Надежда | 
| 2023-10-01 | 12:45 | Chrome 117.0.5938.132 Yandex 23.9.0.2272 | FAIL https://trello.com/c/H9BpaAV0/300 |Samsung Galaxy A50/Chrome 117.0.5938.60  | FAIL https://trello.com/c/H9BpaAV0/300 | 01.10.23 | Наталья К. |

* Продовый сервер

| Дата | Время | Браузер Desktop| Результат/Баг № Trello| Браузер тач| Результат/Баг № Trello| Дата релиза |Имя |
| --- | --- | --- | --- | --- | --- | --- | --- | 
| 2023-07-29 | 20:28 | Chrome 114.0.5735.248 | FAIL https://trello.com/c/H9BpaAV0/300 | Samsung Galaxy A50/Chrome 114.0.5735.196  | FAIL https://trello.com/c/H9BpaAV0/300 | 04.07.23 | Наталья К. | 
| 2023-07-29 | 20:31 | Yandex 23.7.0.2534 | FAIL https://trello.com/c/H9BpaAV0/300 |  |  | 04.07.23 | Наталья К. |
| 14.08.23 | 00:10 | Chrome версия 114.0.5735.199 Firefox версия 115.0.2 | FAIL https://trello.com/c/H9BpaAV0/300 | Chrome версия 114.0.5735.196 MIUI 12.5.13 | FAIL https://trello.com/c/H9BpaAV0/300 | 13.08.23 | Надежда | 
|2023-09-17 | 11.51 | Chrome | FAILED | Chrome, iphone | FAILED | 2023-09-17 | Виктор
| 2023-10-01 | 12:45 | Chrome 117.0.5938.132 Yandex 23.9.0.2272 | FAIL https://trello.com/c/H9BpaAV0/300 |Samsung Galaxy A50/Chrome 117.0.5938.60  | FAIL https://trello.com/c/H9BpaAV0/300 | 01.10.23 | Наталья К. |
| 2023-10-08 | 07:00 | Chrome 117.0.5938.150 Yandex 23.9.0.2272 | FAIL https://trello.com/c/H9BpaAV0/300 |Samsung Galaxy A50/Chrome 117.0.5938.153  | FAIL https://trello.com/c/H9BpaAV0/300 | 08.10.23 | Наталья К. |

