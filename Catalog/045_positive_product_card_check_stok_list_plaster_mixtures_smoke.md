Переход по ссылке "Проверить наличие" товара страницы "Штукатурные смеси".

* Тестовые данные: 

Priority = middle

1. Тестовый сервер - https://test2.stroyrem-nn.ru/
2. Продовый сервер -https://stroyrem-nn.ru/

* Предусловия:
1. Открыта страница "Штукатурные смеси" https://test2.stroyrem-nn.ru/catalog/shtukaturnye-smesi (или https://stroyrem-nn.ru/catalog/shtukaturnye-smesi)
2. Кнопка отображения "список" активирована.

Шаги:
1. Нажать ссылку "Проверить наличие".

* Ожидаемый результат:
Открылось окно с информацией.

2. Закрыть окно с информацией.

* Ожидаемый результат:
Окно закрылось.

Автор: В.Савин


* Тестовый сервер 

| Дата | Время | Браузер Desktop| Результат/Баг № Trello| Браузер тач| Результат/Баг № Trello| Дата релиза |Имя |
| --- | --- | --- | --- | --- | --- | --- | --- | 
|2023-08-01 | 13:07 | Chrome 114.0.5735.248 | PASS |Samsung Galaxy A50/Chrome 114.0.5735.196  | PASS | 04.07.23 | Наталья К. | 
|2023-08-01 | 13:10 | Yandex 23.7.0.2534 | PASS |  |  | 04.07.23 | Наталья К. |
| 14.08.23 | 00:50 | Chrome версия 114.0.5735.199 Firefox версия 115.0.2 | FAIL https://trello.com/c/1qIbzMb7/312 | Chrome версия 114.0.5735.196 MIUI 12.5.13 | FAIL https://trello.com/c/1qIbzMb7/312 | 13.08.23 | Надежда |  
| 2023-10-01 | 12:00 | Chrome 117.0.5938.132 Yandex 23.9.0.2272 | FAIL https://trello.com/c/1qIbzMb7/312 |Samsung Galaxy A50/Chrome 117.0.5938.60  | FAIL https://trello.com/c/1qIbzMb7/312 | 01.10.23 | Наталья К. |

* Продовый сервер

| Дата | Время | Браузер Desktop| Результат/Баг № Trello| Браузер тач| Результат/Баг № Trello| Дата релиза |Имя |
| --- | --- | --- | --- | --- | --- | --- | --- | 
| 2023-08-01 | 13:08 | Chrome 114.0.5735.248 | PASS |Samsung Galaxy A50/Chrome 114.0.5735.196 | PASS | 04.07.23 | Наталья К. | 
| 2023-08-01 | 13:11 | Yandex 23.7.0.2534 | PASS |  |  | 04.07.23 | Наталья К. |
| 14.08.23 | 00:53 | Chrome версия 114.0.5735.199 Firefox версия 115.0.2 | FAIL https://trello.com/c/1qIbzMb7/312 | Chrome версия 114.0.5735.196 MIUI 12.5.13 | FAIL https://trello.com/c/1qIbzMb7/312 | 13.08.23 | Надежда |
| 2023-10-01 | 12:00 | Chrome 117.0.5938.132 Yandex 23.9.0.2272 | FAIL https://trello.com/c/1qIbzMb7/312 |Samsung Galaxy A50/Chrome 117.0.5938.60  | FAIL https://trello.com/c/1qIbzMb7/312 | 01.10.23 | Наталья К. |
| 2023-10-08 | 07:00 | Chrome 117.0.5938.150 Yandex 23.9.0.2272 | FAIL https://trello.com/c/1qIbzMb7/312 |Samsung Galaxy A50/Chrome 117.0.5938.153  | FAIL https://trello.com/c/1qIbzMb7/312 | 08.10.23 | Наталья К. |  




