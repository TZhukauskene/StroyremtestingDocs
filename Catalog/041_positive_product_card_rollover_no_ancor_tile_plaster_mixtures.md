Отображение курсора над фото товара страницы "Штукатурные смеси" (Desktop).

* Тестовые данные: 

Priority = low

1. Тестовый сервер - https://test2.stroyrem-nn.ru/
2. Продовый сервер -https://stroyrem-nn.ru/

* Предусловия:
1. Открыта страница "Штукатурные смеси" https://test2.stroyrem-nn.ru/catalog/shtukaturnye-smesi (или https://stroyrem-nn.ru/catalog/shtukaturnye-smesi)
2. Кнопка отображения "плитка" активирована.

Шаги:
1. Навести курсор на код товара.

* Ожидаемый результат:
Курсор отображается не как наведенный на ссылку.

2. Навести курсор на остаток товара.

* Ожидаемый результат:
Курсор отображается не как наведенный на ссылку.

3. Навести курсор на кнопку с единицами измерения товара (мешок).

* Ожидаемый результат:
Курсор отображается не как наведенный на ссылку.

4. Навести курсор на цену товара.

* Ожидаемый результат:
Курсор отображается не как наведенный на ссылку.

4. Навести курсор на название товара.

* Ожидаемый результат:
Курсор отображается как наведенный на ссылку.

Автор: В.Савин


* Тестовый сервер 

| Дата | Время | Браузер Desktop| Результат/Баг № Trello| Браузер тач| Результат/Баг № Trello| Дата релиза |Имя |
| --- | --- | --- | --- | --- | --- | --- | --- | 
|2023-07-29 | 21:27 | Chrome 114.0.5735.248 | FAIL https://trello.com/c/RLWHtfBB/301 |  | | 04.07.23 | Наталья К. | 
|2023-07-29 | 21:30 | Yandex 23.7.0.2534 | FAIL https://trello.com/c/RLWHtfBB/301 |  |  | 04.07.23 | Наталья К. |
| 13.08.23 | 02:03 | Chrome версия 114.0.5735.199 Firefox версия 115.0.2 | FAIL https://trello.com/c/RLWHtfBB/301 | | | 13.08.23 | Надежда |
| 2023-10-01 | 14:00 | Chrome 117.0.5938.132 Yandex 23.9.0.2272 | FAIL https://trello.com/c/RLWHtfBB/301 | |  | 01.10.23 | Наталья К. | 


* Продовый сервер

| Дата | Время | Браузер Desktop| Результат/Баг № Trello| Браузер тач| Результат/Баг № Trello| Дата релиза |Имя |
| --- | --- | --- | --- | --- | --- | --- | --- | 
| 2023-07-29 | 21:28 | Chrome 114.0.5735.248 | FAIL https://trello.com/c/RLWHtfBB/301 | | | 04.07.23 | Наталья К. | 
| 2023-07-29 | 21:31 | Yandex 23.7.0.2534 | FAIL https://trello.com/c/RLWHtfBB/301 |  |  | 04.07.23 | Наталья К. |
| 14.08.23 | 00:13 | Chrome версия 114.0.5735.199 Firefox версия 115.0.2 | FAIL https://trello.com/c/RLWHtfBB/301 | | | 13.08.23 | Надежда |
| 2023-10-01 | 14:00 | Chrome 117.0.5938.132 Yandex 23.9.0.2272 | FAIL https://trello.com/c/RLWHtfBB/301 | |  | 01.10.23 | Наталья К. |
| 2023-10-08 | 07:00 | Chrome 117.0.5938.150 Yandex 23.9.0.2272 | FAIL https://trello.com/c/RLWHtfBB/301 | | | 08.10.23 | Наталья К. | 

