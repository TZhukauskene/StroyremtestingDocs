Проверка вкладки "ОТЗЫВЫ" подробной карточки товара(Desktop).

* Тестовые данные: 

1. Тестовый сервер - https://test2.stroyrem-nn.ru/
2. Продовый сервер -https://stroyrem-nn.ru/

* Предусловия:
1. Выбрана карточка товара с уже имеющимися отзывами.
2. Открыта страница подробной карточки товара. 


Шаги:
1. Нажать на поле "*****" (отзывы).

* Ожидаемый результат:
Перемещение к вкладке "Отзывы" на данной странице.

2. Нажимаем на любую другую вкладку.

* Ожидаемый результат:
Перемещение в другую вкладку на данной странице.

3. Нажимаем на вкладку "ОТЗЫВЫ +...".

* Ожидаемый результат:
Перемещение в вкладку "Отзывы" на данной странице.

4. Сравним цифры (отображение количества отзывов,например "4").

* Ожидаемый результат:
- цифра у "*****(4)" совпадает с цифрой у "ОТЗЫВЫ +4" и цифрой у строки "Кол-во отзывов: 4"

5. Сравнить количество карточек отзывов с цифрой у строки "Кол-во отзывов: ..."

* Ожидаемый результат:
Количество карточек с отзывами совпадает с цифрой в строке "Кол-во отзывов: ..."



Автор: Н.Краснобородько


* Тестовый сервер

| Дата | Время | Браузер Desktop| Результат/Баг № Trello| Браузер тач| Результат/Баг № Trello| Дата релиза |Имя |
| --- | --- | --- | --- | --- | --- | --- | --- | 
|2023-08-26 | 09:20 | Chrome 116.0.5845.111  Yandex 23.7.3.824| https://trello.com/c/jhYDoOaC/419 | |  | 13.08.23 | Наталья К. |
| 2023-10-01 | 18:45 | Chrome 117.0.5938.132 Yandex 23.9.0.2272 | https://trello.com/c/jhYDoOaC/419 | | | 01.10.23 | Наталья К. | 



* Продовый сервер

| Дата | Время | Браузер Desktop| Результат/Баг № Trello| Браузер тач| Результат/Баг № Trello| Дата релиза |Имя |
| --- | --- | --- | --- | --- | --- | --- | --- | 
| 2023-08-26 | 09:20 | Chrome 116.0.5845.111 Yandex 23.7.3.824 | PASS  | |  | 13.08.23 | Наталья К. |
| 2023-09-17 | 17:10 | Chrome 116.0.5845.188 Yandex 23.7.5.704 | PASS  | |  | 17.09.23 | Наталья К. |
| 2023-10-01 | 18:45 | Chrome 117.0.5938.132 Yandex 23.9.0.2272 | PASS | |  | 01.10.23 | Наталья К. |
| 2023-10-08 | 07:00 | Chrome 117.0.5938.150 Yandex 23.9.0.2272 | PASS | |  | 08.10.23 | Наталья К. |   

