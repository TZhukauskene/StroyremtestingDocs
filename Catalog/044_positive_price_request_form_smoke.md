Форма запроса цены.

* Тестовые данные: 

Priority = high

1. Тестовый сервер - https://test2.stroyrem-nn.ru/
2. Продовый сервер -https://stroyrem-nn.ru/

* Предусловия:
1. Открыть страницу "Штукатурные смеси" https://test2.stroyrem-nn.ru/catalog/shtukaturnye-smesi (или https://stroyrem-nn.ru/catalog/shtukaturnye-smesi)
2. В карточке товара нажать кнопку "ЗАПРОСИТЬ ЦЕНУ".

Шаги:
1. В поле "Ваше имя" ввести "Тест Тест".
2. В поле "Ваш телефон" ввести +7(996)1058268.
3. Нажать "Отправить заявку".

* Ожидаемый результат:
Заявка отправлена.

Автор: В.Савин


* Тестовый сервер 

| Дата | Время | Браузер Desktop| Результат/Баг № Trello| Браузер тач| Результат/Баг № Trello| Дата релиза |Имя |
| --- | --- | --- | --- | --- | --- | --- | --- | 
|2023-07-30 | 19:07 | Chrome 114.0.5735.248 | FAIL https://trello.com/c/87G2TPyC/305 |Samsung Galaxy A50/Chrome 114.0.5735.196  | FAIL https://trello.com/c/87G2TPyC/305 | 04.07.23 | Наталья К. | 
|2023-07-30 | 19:10 | Yandex 23.7.0.2534 | FAIL https://trello.com/c/87G2TPyC/305 |  |  | 04.07.23 | Наталья К. |
| 14.08.23 | 00:44 | Chrome версия 114.0.5735.199 Firefox версия 115.0.2 | FAIL https://trello.com/c/87G2TPyC/305 | Chrome версия 114.0.5735.196 MIUI 12.5.13 | FAIL https://trello.com/c/87G2TPyC/305 | 13.08.23 | Надежда |  
| 2023-10-01 | 16:30 | Chrome 117.0.5938.132 Yandex 23.9.0.2272 | PASS |Samsung Galaxy A50/Chrome 117.0.5938.60  | PASS | 01.10.23 | Наталья К. |

* Продовый сервер

| Дата | Время | Браузер Desktop| Результат/Баг № Trello| Браузер тач| Результат/Баг № Trello| Дата релиза |Имя |
| --- | --- | --- | --- | --- | --- | --- | --- | 
| 2023-07-30 | 19:08 | Chrome 114.0.5735.248 | FAIL https://trello.com/c/87G2TPyC/305 |Samsung Galaxy A50/Chrome 114.0.5735.196 | FAIL https://trello.com/c/87G2TPyC/305 | 04.07.23 | Наталья К. | 
| 2023-07-30 | 19:11 | Yandex 23.7.0.2534 | FAIL https://trello.com/c/87G2TPyC/305 |  |  | 04.07.23 | Наталья К. |
| 14.08.23 | 00:47 | Chrome версия 114.0.5735.199 Firefox версия 115.0.2 | FAIL https://trello.com/c/87G2TPyC/305 | Chrome версия 114.0.5735.196 MIUI 12.5.13 | FAIL https://trello.com/c/87G2TPyC/305 | 13.08.23 | Надежда |
| 2023-09-17 | 22:00 | Chrome 116.0.5845.188 Yandex 23.7.5.704 | PASS |Samsung Galaxy A50/Chrome 117.0.5938.60  | PASS | 17.09.23 | Наталья К. | 
| 2023-10-01 | 16:30 | Chrome 117.0.5938.132 Yandex 23.9.0.2272 | PASS |Samsung Galaxy A50/Chrome 117.0.5938.60  | PASS | 01.10.23 | Наталья К. | 
| 2023-10-08 | 07:00 | Chrome 117.0.5938.150 Yandex 23.9.0.2272 | PASS |Samsung Galaxy A50/Chrome 117.0.5938.153  | PASS | 08.10.23 | Наталья К. |

