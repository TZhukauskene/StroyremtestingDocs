Открытие первой ссылки каталога.

* Тестовые данные: 
1. Тестовый сервер - https://test2.stroyrem-nn.ru/
2. Продовый сервер -https://stroyrem-nn.ru/

* Предусловия:
1. Открыта главная страница;
2. В меню каталога товаров открыто "Гипсокартонные системы".

* Шаги:
Нажать гипсокартонные листы (ГКЛ)

* Ожидаемый результат:
Открылась страница "Гипсокартонные листы (ГКЛ)".

Автор: В.Савин

* Тестовый сервер 

| Дата | Время | Браузер Desktop| Результат/Баг № Trello| Браузер тач| Результат/Баг № Trello| Дата релиза |Имя |
| --- | --- | --- | --- | --- | --- | --- | --- | 
|2023-07-18 | 18:07 | Chrome 114.0.5735.199 | PASS | Samsung Galaxy A50/Chrome 114.0.5735.196 | PASS | 04.07.23 | Наталья К. | 
|2023-07-18 | 18:10 | Yandex 23.5.4.674 | PASS |  |  | 04.07.23 | Наталья К. |
 12.08.23 | 10:49 | Chrome версия 114.0.5735.199 Firefox версия 115.0.2 | PASS | Chrome версия 114.0.5735.196 MIUI 12.5.13 | PASS | 16.06.23 | Надежда |
| 2023-09-28 | 18:30 | Chrome 117.0.5938.92 Yandex 23.7.5.739 | PASS |Samsung Galaxy A50/Chrome 117.0.5938.60  | PASS | 17.09.23 | Наталья К. | 
| 2023-10-06 | 14:30 | Chrome 117.0.5938.150 Firefox 118.0.1 | PASS| Chrome 117.0.5938.60, Android 10 |PASS| 01.10.2023  | Татьяна |


Продовый сервер

| Дата | Время | Браузер Desktop| Результат/Баг № Trello| Браузер тач| Результат/Баг № Trello| Дата релиза |Имя |
| --- | --- | --- | --- | --- | --- | --- | --- | 
| 2023-07-18 | 18:08 | Chrome 114.0.5735.199 | PASS | Samsung Galaxy A50/Chrome 114.0.5735.196 | PASS | 04.07.23 | Наталья К. | 
| 2023-07-18 | 18:11 | Yandex 23.5.4.674 | PASS |  |  | 04.07.23 | Наталья К. |
 13.08.23 | 20:05 | Chrome версия 114.0.5735.199 Firefox версия 115.0.2 | PASS | Chrome версия 114.0.5735.196 MIUI 12.5.13 | PASS | 13.08.23 | Надежда |  
|2023-09-17 | 08:23 | Chrome | PASS | Chrome, iphone | PASS | 2023-09-17 | Виктор
| 2023-10-01 | 9:30 | Chrome 117.0.5938.132 Yandex 23.9.0.2272 | PASS |Samsung Galaxy A50/Chrome 117.0.5938.60  | PASS | 01.10.23 | Наталья К. |
| 2023-10-08 | 07:00 | Chrome 117.0.5938.150 Yandex 23.9.0.2272 | PASS |Samsung Galaxy A50/Chrome 117.0.5938.153  | PASS | 08.10.23 | Наталья К. |
