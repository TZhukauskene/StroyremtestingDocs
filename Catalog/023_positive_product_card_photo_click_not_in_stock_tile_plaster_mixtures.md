Переход на страницу описания товара через фото товара "Нет на складе" на странице "Штукатурные смеси".

* Тестовые данные: 

Priority = high

1. Тестовый сервер - https://test2.stroyrem-nn.ru/
2. Продовый сервер -https://stroyrem-nn.ru/

* Предусловия:
1. Открыта страница "Штукатурные смеси" https://test2.stroyrem-nn.ru/catalog/shtukaturnye-smesi (или https://stroyrem-nn.ru/catalog/shtukaturnye-smesi)
2. Кнопка отображения "плитка" активирована.

Шаги:
1. Нажать на фото товара, где написано "нет на складе".

* Ожидаемый результат:
Открылась страница с описанием выбранного товара.

Автор: В.Савин


* Тестовый сервер 

| Дата | Время | Браузер Desktop| Результат/Баг № Trello| Браузер тач| Результат/Баг № Trello| Дата релиза |Имя |
| --- | --- | --- | --- | --- | --- | --- | --- | 
|2023-07-25 | 16:07 | Chrome 114.0.5735.248 | PASS | Samsung Galaxy A50/Chrome 114.0.5735.196 | PASS | 04.07.23 | Наталья К. | 
|2023-07-25 | 16:10 | Yandex 23.5.4.674 | PASS |  |  | 04.07.23 | Наталья К. |
| 12.08.23 | 18:50 | Chrome версия 114.0.5735.199 Firefox версия 115.0.2 | PASS | Chrome версия 114.0.5735.196 MIUI 12.5.13 | PASS | 16.06.23 | Надежда |
| 2023-09-29 | 22:10 | Chrome 117.0.5938.132 Yandex 23.9.0.2272 | PASS |Samsung Galaxy A50/Chrome 117.0.5938.60  | PASS  | 17.09.23 | Наталья К. |  


* Продовый сервер

| Дата | Время | Браузер Desktop| Результат/Баг № Trello| Браузер тач| Результат/Баг № Trello| Дата релиза |Имя |
| --- | --- | --- | --- | --- | --- | --- | --- | 
| 2023-07-25 | 16:08 | Chrome 114.0.5735.248 | PASS | Samsung Galaxy A50/Chrome 114.0.5735.196 | PASS | 04.07.23 | Наталья К. | 
| 2023-07-25 | 16:11 | Yandex 23.7.0.2534 | PASS |  |  | 04.07.23 | Наталья К. |
| 13.08.23 | 22:38 | Chrome версия 114.0.5735.199 Firefox версия 115.0.2 | PASS | Chrome версия 114.0.5735.196 MIUI 12.5.13 | PASS | 13.08.23 | Надежда |  
|2023-09-17 | 10.59 | Chrome | PASSED | Chrome, iphone | PASSED | 2023-09-17 | Виктор
| 2023-10-01 | 11:10 | Chrome 117.0.5938.132 Yandex 23.9.0.2272 | PASS |Samsung Galaxy A50/Chrome 117.0.5938.60  | PASS | 01.10.23 | Наталья К. |
| 2023-10-08 | 07:00 | Chrome 117.0.5938.150 Yandex 23.9.0.2272 | PASS |Samsung Galaxy A50/Chrome 117.0.5938.153  | PASS | 08.10.23 | Наталья К. |