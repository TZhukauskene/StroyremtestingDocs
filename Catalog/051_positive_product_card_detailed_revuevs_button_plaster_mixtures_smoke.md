Проверка блока отзывов подробной карточки товара страницы "Штукатурные смеси".

* Тестовые данные: 

Priority = high

1. Тестовый сервер - https://test2.stroyrem-nn.ru/
2. Продовый сервер -https://stroyrem-nn.ru/

* Предусловия:
1. Открыта страница товара 
https://test2.stroyrem-nn.ru/products/shtukaturka-tsementnaya-zaschitnaya-tonkoslojnaya-knauf-virton-25-kg-42 
или 
https://stroyrem-nn.ru/products/shtukaturka-tsementnaya-zaschitnaya-tonkoslojnaya-knauf-virton-25-kg-42
2. Открыта вкладка "ОТЗЫВЫ"

1. Шаги:
Нажать кнопку "Написать отзыв".

* Ожидаемый результат:
Появилась форма написания отзыва.

3. Шаги:
Закрыть окно "Написать отзыв"

* Ожидаемый результат:
Окно закрылось.

Автор: В.Савин


* Тестовый сервер 

| Дата | Время | Браузер Desktop| Результат/Баг № Trello| Браузер тач| Результат/Баг № Trello| Дата релиза |Имя |
| --- | --- | --- | --- | --- | --- | --- | --- | 
|2023-08-01 | 20:27 | Chrome 114.0.5735.248 | PASS |Samsung Galaxy A50/Chrome 114.0.5735.196  | FAIL https://trello.com/c/SshXUPEN/319 | 04.07.23 | Наталья К. | 
|2023-08-01 | 20:30 | Yandex 23.7.0.2534 | PASS |  |  | 04.07.23 | Наталья К. |
| 14.08.23 | 02:07 | Chrome версия 114.0.5735.199 Firefox версия 115.0.2 | PASS | Chrome версия 114.0.5735.196 MIUI 12.5.13 | FAIL https://trello.com/c/SshXUPEN/319 | 13.08.23 | Надежда |  
| 2023-09-17 | 18:00 | Chrome 116.0.5845.188 Yandex 23.7.5.704 | PASS |Samsung Galaxy A50/Chrome 117.0.5938.60  | FAIL https://trello.com/c/0ayFGW8B/599 | 17.09.23 | Наталья К. |

* Продовый сервер

| Дата | Время | Браузер Desktop| Результат/Баг № Trello| Браузер тач| Результат/Баг № Trello| Дата релиза |Имя |
| --- | --- | --- | --- | --- | --- | --- | --- | 
| 2023-08-01 | 20:28 | Chrome 114.0.5735.248 | PASS |Samsung Galaxy A50/Chrome 114.0.5735.196 | FAIL https://trello.com/c/SshXUPEN/319 | 04.07.23 | Наталья К. | 
| 2023-08-01 | 20:31 | Yandex 23.7.0.2534 | PASS |  |  | 04.07.23 | Наталья К. |
| 14.08.23 | 02:10 | Chrome версия 114.0.5735.199 Firefox версия 115.0.2 | PASS  | Chrome версия 114.0.5735.196 MIUI 12.5.13 | FAIL https://trello.com/c/SshXUPEN/319 | 13.08.23 | Надежда |
| 2023-09-17 | 18:00 | Chrome 116.0.5845.188 Yandex 23.7.5.704 | PASS |Samsung Galaxy A50/Chrome 117.0.5938.60  | FAIL https://trello.com/c/0ayFGW8B/599 | 17.09.23 | Наталья К. |
| 2023-10-08 | 07:00 | Chrome 117.0.5938.150 Yandex 23.9.0.2272 | PASS |Samsung Galaxy A50/Chrome 117.0.5938.153  | FAIL https://trello.com/c/0ayFGW8B/599 | 08.10.23 | Наталья К. |   

