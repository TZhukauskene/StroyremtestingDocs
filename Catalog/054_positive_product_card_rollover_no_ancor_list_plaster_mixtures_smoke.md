Отображение курсора над фото товара страницы "Штукатурные смеси" (Desktop).

* Тестовые данные: 

Priority = low

1. Тестовый сервер - https://test2.stroyrem-nn.ru/
2. Продовый сервер -https://stroyrem-nn.ru/

* Предусловия:
1. Открыта страница "Штукатурные смеси" https://test2.stroyrem-nn.ru/catalog/shtukaturnye-smesi (или https://stroyrem-nn.ru/catalog/shtukaturnye-smesi)
2. Кнопка отображения "список" активирована.

Шаги:
1. Навести курсор на описание товара.

* Ожидаемый результат:
Курсор отображается как обычно.

Автор: В.Савин


* Тестовый сервер 

| Дата | Время | Браузер Desktop| Результат/Баг № Trello| Браузер тач| Результат/Баг № Trello| Дата релиза |Имя |
| --- | --- | --- | --- | --- | --- | --- | --- | 
|2023-18-02 | 13:37 | Chrome 115.0.5790.110| PASS | |  | 04.07.23 | Наталья К. | 
|2023-18-02 | 13:40 | Yandex 23.7.1.1140 | PASS |  |  | 04.07.23 | Наталья К. |
| 14.08.23 | 02:29 | Chrome версия 114.0.5735.199 Firefox версия 115.0.2 | PASS | | | 13.08.23 | Надежда |
| 2023-10-01 | 18:15 | Chrome 117.0.5938.132 Yandex 23.9.0.2272 | PASS |Samsung Galaxy A50/Chrome 117.0.5938.60  | PASS | 01.10.23 | Наталья К. |   


* Продовый сервер

| Дата | Время | Браузер Desktop| Результат/Баг № Trello| Браузер тач| Результат/Баг № Trello| Дата релиза |Имя |
| --- | --- | --- | --- | --- | --- | --- | --- | 
| 2023-18-02 | 13:38 | Chrome 115.0.5790.110 | PASS | |  | 04.07.23 | Наталья К. | 
| 2023-18-02 | 13:41 | Yandex 23.7.1.1140 | PASS |  |  | 04.07.23 | Наталья К. |
| 14.08.23 | 02:33 | Chrome версия 114.0.5735.199 Firefox версия 115.0.2 | PASS | | | 13.08.23 | Надежда |
| 2023-10-01 | 18:15 | Chrome 117.0.5938.132 Yandex 23.9.0.2272 | PASS |  |  | 01.10.23 | Наталья К. |
| 2023-10-08 | 07:00 | Chrome 117.0.5938.150 Yandex 23.9.0.2272 | PASS |  |  | 08.10.23 | Наталья К. |   
