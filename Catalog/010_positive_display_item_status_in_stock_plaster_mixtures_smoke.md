Изменение статуса товара "на складе" на странице "Штукатурные смеси".

* Тестовые данные: 
1. Тестовый сервер - https://test2.stroyrem-nn.ru/
2. Продовый сервер -https://stroyrem-nn.ru/

* Предусловия:
1.Открыта страница "Штукатурные смеси" https://test2.stroyrem-nn.ru/catalog/shtukaturnye-smesi (или https://stroyrem-nn.ru/catalog/shtukaturnye-smesi)
2. Нажата кнопка "Фильтр" (тач)

Шаги:
1. Нажать кнопку "На складе".
2. Нажать кнопку "Показать  ... товаров" (тач)

*Ожидаемый результат:
Отображен товар (штукатурные смеси), присутствующий на складе.

3. Нажата кнопка "Фильтр" (тач)
4. Нажать кнопку "На складе".
5. Нажать кнопку "Показать  ... товаров" (тач)

*Ожидаемый результат:
Отображен весь товар (штукатурные смеси).

Автор: В.Савин

Отчет о тестировании

Тестовый сервер

| Дата | Время | Браузер Desktop| Результат/Баг № Trello| Браузер тач| Результат/Баг № Trello| Дата релиза |Имя |
| --- | --- | --- | --- | --- | --- | --- | --- | 
|2023-07-24 | 21:06 | Chrome 114.0.5735.248 | PASS | Samsung Galaxy A50/Chrome 114.0.5735.196 | PASS | 04.07.23 | Наталья К. | 
|2023-07-21 | 21:08 | Yandex 23.5.4.674 | PASS |  |  | 04.07.23 | Наталья К. |
 12.08.23 | 12:40 | Chrome версия 114.0.5735.199 Firefox версия 115.0.2 | PASS | Chrome версия 114.0.5735.196 MIUI 12.5.13 | PASS | 16.06.23 | Надежда | 
| 2023-09-29 | 21:05 | Chrome 117.0.5938.132 Yandex 23.9.0.2272 | FAIL https://trello.com/c/dov6ZOMD/593  |Samsung Galaxy A50/Chrome 117.0.5938.60  | FAIL https://trello.com/c/dov6ZOMD/593   | 17.09.23 | Наталья К. | 
| 2023-10-06 | 15:35 | Chrome 117.0.5938.150 Firefox 118.0.1 | PASS| Chrome 117.0.5938.60, Android 10 |PASS| 01.10.2023  | Татьяна |

Продовый сервер

| Дата | Время | Браузер Desktop| Результат/Баг № Trello| Браузер тач| Результат/Баг № Trello| Дата релиза |Имя |
| --- | --- | --- | --- | --- | --- | --- | --- | 
| 2023-07-24 | 21:07 | Chrome 114.0.5735.248 | FAIL https://trello.com/c/L2aAXVNy/238| Samsung Galaxy A50/Chrome 114.0.5735.196 | FAIL https://trello.com/c/L2aAXVNy/238 | 04.07.23 | Наталья К. | 
| 2023-07-24 | 21:09 | Yandex 23.7.0.2534 | FAIL https://trello.com/c/L2aAXVNy/238|  |  | 04.07.23 | Наталья К. |
 13.08.23 | 20:30 | Chrome версия 114.0.5735.199 Firefox версия 115.0.2 | FAIL https://trello.com/c/L2aAXVNy/238 | Chrome версия 114.0.5735.196 MIUI 12.5.13 | FAIL https://trello.com/c/L2aAXVNy/238 | 13.08.23 | Надежда |  
|2023-09-17 | 10.58| Chrome | SKIPED | Chrome, iphone | SKIPED | 2023-09-17 | Виктор
| 2023-10-01 | 9:45 | Chrome 117.0.5938.132 Yandex 23.9.0.2272 | FAIL https://trello.com/c/L2aAXVNy/238 |Samsung Galaxy A50/Chrome 117.0.5938.60  | FAIL https://trello.com/c/L2aAXVNy/238 | 01.10.23 | Наталья К. |
| 2023-10-08 | 07:00 | Chrome 117.0.5938.150 Yandex 23.9.0.2272 | FAIL https://trello.com/c/L2aAXVNy/238 |Samsung Galaxy A50/Chrome 117.0.5938.153  | FAIL https://trello.com/c/L2aAXVNy/238 | 08.10.23 | Наталья К. |
