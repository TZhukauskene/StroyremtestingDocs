Regjur29

Регистрация нового пользователя (юр лицо) с введенными невалидными данными в поле "Корреспондентский счёт" (данные о счете организации)

* Тестовые данные: 
  
  Предусловия:
  
  1. Создан тестовый электронный ящик(и)
  
  2. Открыта форма регистрации и выбрана вкладка "Юридическое лицо"
     
     (ссылка на прод [Регистрация](https://stroyrem-nn.ru/user/register) и на тест [Регистрация](https://test2.stroyrem-nn.ru/user/register))
  
  3. Все поля заполнены валидными данными, кроме поля "Корреспондентский счёт" (данные о счете организации)

* Шаги:
1. В поле "Корреспондентский счёт" (данные о счете организации) напечатать несколько пробелов и нажать кнопку "Зарегистрироваться"
   
   **ОР:** Поле "Корреспондентский счёт" в рамке красного цвета и надпись "Только цифры"

2. В поле "Корреспондентский счёт" (данные о счете организации) ввести 20 рандомных арабских цифр
   
   **ОР:** Поле "Корреспондентский счёт" в рамке красного цвета и надпись "Должно начинаться с 30101"

3. В поле "Корреспондентский счёт" (Данные о счете организации) ввести специальные символы (например,  !@#%^&*$<>) и нажать кнопку "Зарегистрироваться"
   
   **ОР:** Поле "Корреспондентский счёт" в рамке красного цвета и надпись "Только цифры"

4. В поле "Корреспондентский счёт" (Данные о счете организации) ввести кириллицу и нажать кнопку "Зарегистрироваться"
   
   **ОР:** Поле "Корреспондентский счёт" в рамке красного цвета и надпись "Только цифры"

5. В поле "Корреспондентский счёт" (Данные о счете организации) ввести латинские буквы и нажать кнопку "Зарегистрироваться"
   
   **ОР:** Поле "Корреспондентский счёт" в рамке красного цвета и надпись "Только цифры"

6. В поле "Корреспондентский счёт" (Данные о счете организации) ввести арабские цифры и знаки +- и нажать кнопку "Зарегистрироваться"
   
   **ОР:** Поле "Корреспондентский счёт" в рамке красного цвета и надпись "Только цифры"

7. В поле "Корреспондентский счёт" (Данные о счете организации) ввести рандомные арабские цифры от 0 до 9 больше 20 цифр и нажать кнопку "Зарегистрироваться"
   
   **ОР:** Поле "Корреспондентский счёт" в рамке красного цвета и надпись "Максимально 20 чисел"

* Постусловия: удалить тестовые данные на тестовом сервере/выйти из профиля, если регистрация проходит успешно

Автор: Татьяна

Отчет о тестировании

Тестовый сервер 

| № Шага | Дата       | Время | Версия браузера Десктоп| Результат/Баг в Трелло Десктоп | Версия браузера и ОС Тач| Результат/Баг в Трелло Тач | Дата релиза | QA |
| ---------- | ---------- | ----- | ----------------------------------- | ---------------------------------- | -------------------------------- | ---------------------------------- | ----------- | ------- |
| 1   | 2023-08-02 | 15:35 |Chrome 115.0.5790.110 Firefox 115.0.3| FAIL https://trello.com/c/3tbWgErd   | Android 10 | FAIL https://trello.com/c/3tbWgErd  | 16.06.23 | Татьяна |
| 2   | 2023-08-02 | 15:49 |Chrome 115.0.5790.110 Firefox 115.0.3| FAIL https://trello.com/c/4iGyu8i1  | Android 10 | FAIL https://trello.com/c/4iGyu8i1   | 16.06.23 | Татьяна |
| 2 |2023-10-06 | 19:11 | Chrome 117.0.5938.89 | FAIL https://trello.com/c/4iGyu8i1 | Safari 16.6.1 | FAIL https://trello.com/c/4iGyu8i1 | --- | Мария |
| 3   | 2023-08-02 | 16:00 |Chrome 115.0.5790.110 Firefox 115.0.3| FAIL https://trello.com/c/K7cnul1Z   | Android 10 | FAIL https://trello.com/c/K7cnul1Z   | 16.06.23 | Татьяна |
| 4   | 2023-08-02 | 16:05 |Chrome 115.0.5790.110 Firefox 115.0.3| FAIL https://trello.com/c/nctvd2e3   | Android 10 | FAIL https://trello.com/c/nctvd2e3   | 16.06.23 | Татьяна |
| 5   | 2023-08-02 | 16:29 |Chrome 115.0.5790.110 Firefox 115.0.3| FAIL https://trello.com/c/ZaxYS7d5   | Android 10 | FAIL https://trello.com/c/ZaxYS7d5  | 16.06.23 | Татьяна |
| 6   | 2023-08-02 | 16:40 |Chrome 115.0.5790.110 Firefox 115.0.3| FAIL https://trello.com/c/rv9I94H2   | Android 10 | FAIL https://trello.com/c/rv9I94H2   | 16.06.23 | Татьяна |
| 7   | 2023-08-02 | 16:50 |Chrome 115.0.5790.110 Firefox 115.0.3| FAIL https://trello.com/c/yY666mkW   | Android 10 | FAIL https://trello.com/c/yY666mkW   | 16.06.23 | Татьяна |
| 7 |2023-10-06 | 19:11 | Chrome 117.0.5938.89 | FAIL https://trello.com/c/yY666mkW | Safari 16.6.1 | FAIL https://trello.com/c/yY666mkW | --- | Мария |

Продовый сервер

| № Шага | Дата       | Время | Версия браузера Десктоп| Результат/Баг в Трелло Десктоп | Версия браузера и ОС Тач| Результат/Баг в Трелло Тач | Дата релиза | QA |
| ---------- | ---------- | ----- | ----------------------------------- | ---------------------------------- | -------------------------------- | ---------------------------------- | ----------- | ------- |
| 1   | 2023-08-02 | 15:36 |Chrome 115.0.5790.110 Firefox 115.0.3| FAIL https://trello.com/c/3tbWgErd   | Android 10 | FAIL https://trello.com/c/3tbWgErd  | 16.06.23 | Татьяна |
|1 |2023-09-20 |12:05|Chrome 117.0.5938.89 Firefox 117.0.1 |PASS   |Chrome 116.0.5845.172 Android 10  |PASS |17.09.23|Татьяна  |
| 2   | 2023-08-02 | 15:49 |Chrome 115.0.5790.110 Firefox 115.0.3| FAIL https://trello.com/c/4iGyu8i1  | Android 10 | FAIL https://trello.com/c/4iGyu8i1   | 16.06.23 | Татьяна |
|2|2023-09-20 |12:07|Chrome 117.0.5938.89 Firefox 117.0.1 |FAIL https://trello.com/c/4iGyu8i1   |Chrome 116.0.5845.172 Android 10  |FAIL https://trello.com/c/4iGyu8i1 |17.09.23|Татьяна  |
| 3   | 2023-08-02 | 16:00 |Chrome 115.0.5790.110 Firefox 115.0.3| FAIL https://trello.com/c/K7cnul1Z   | Android 10 | FAIL https://trello.com/c/K7cnul1Z   | 16.06.23 | Татьяна |
|3 |2023-09-20 |12:13|Chrome 117.0.5938.89 Firefox 117.0.1 |PASS   |Chrome 116.0.5845.172 Android 10  |PASS |17.09.23|Татьяна  |
| 4   | 2023-08-02 | 16:05 |Chrome 115.0.5790.110 Firefox 115.0.3|FAIL https://trello.com/c/nctvd2e3| Android 10 | FAIL https://trello.com/c/nctvd2e3   | 16.06.23 | Татьяна |
|4 |2023-09-20 |12:10|Chrome 117.0.5938.89 Firefox 117.0.1 |PASS   |Chrome 116.0.5845.172 Android 10  |FAIL https://trello.com/c/1HD5Oedq |17.09.23|Татьяна  |
| 5   | 2023-08-02 | 16:30 |Chrome 115.0.5790.110 Firefox 115.0.3| FAIL https://trello.com/c/ZaxYS7d5   | Android 10 | FAIL https://trello.com/c/ZaxYS7d5  | 16.06.23 | Татьяна |
|5 |2023-09-20 |12:15|Chrome 117.0.5938.89 Firefox 117.0.1 |PASS   |Chrome 116.0.5845.172 Android 10  |PASS |17.09.23|Татьяна  |
| 6   | 2023-08-02 | 16:41 |Chrome 115.0.5790.110 Firefox 115.0.3| FAIL https://trello.com/c/rv9I94H2   | Android 10 | FAIL https://trello.com/c/rv9I94H2   | 16.06.23 | Татьяна |
|6 |2023-09-20 |12:17|Chrome 117.0.5938.89 Firefox 117.0.1 |PASS   |Chrome 116.0.5845.172 Android 10  |PASS |17.09.23|Татьяна  |
| 7   | 2023-08-02 | 16:51 |Chrome 115.0.5790.110 Firefox 115.0.3| FAIL https://trello.com/c/yY666mkW   | Android 10 | FAIL https://trello.com/c/yY666mkW   | 16.06.23 | Татьяна |
|7 |2023-09-20 |12:20|Chrome 117.0.5938.89 Firefox 117.0.1 |FAIL https://trello.com/c/yY666mkW   |Chrome 116.0.5845.172 Android 10  |FAIL https://trello.com/c/yY666mkW |17.09.23|Татьяна  |

